# -*- coding: utf-8 -*-

import datetime
import time
import os

# Descargar libreria Mastodon en: https://github.com/halcy/Mastodon.py
# pip3 install Mastodon.py
from mastodon import Mastodon
  
def cargar_datos():
    f = open ('datos.txt','r')
    datos = f.readlines()
    f.close()
    for i in range(len(datos)):
        datos[i] = datos[i].strip()
    return datos
    
API_BASE_URL, CUENTA_CORREO, CUENTA_PASS, ANTIGUEDAD_DIAS, RETARDO_SEG, SALIR = cargar_datos()
SALIR = int(SALIR)
RETARDO_SEG = float(RETARDO_SEG)
ANTIGUEDAD_DIAS = int(ANTIGUEDAD_DIAS)

if not os.path.exists('pytooter_clientcred.secret'):
    Mastodon.create_app(
        'pytooterapp',
        api_base_url = API_BASE_URL,
        to_file = 'pytooter_clientcred.secret'
    )
 
mastodon = Mastodon(
    client_id = 'pytooter_clientcred.secret',
    api_base_url = API_BASE_URL
)
 
mastodon.log_in(
    CUENTA_CORREO,
    CUENTA_PASS,
    to_file = 'pytooter_usercred.secret'
)

fechaRestada = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=ANTIGUEDAD_DIAS)
vueltas = 0

while SALIR>vueltas:
    resp = mastodon.favourites(limit=100, min_id=0)

    for fav in resp[::-1]:
        print('')
        print(fav.id)
        print(fav.created_at)
        if fav.created_at < fechaRestada:
            print('El fav es mas antiguo que:')
            print(fechaRestada)
            # a borrar!
            mastodon.status_unfavourite(fav.id)
        else:
            vueltas = SALIR
            break
        time.sleep(RETARDO_SEG)
    vueltas = vueltas + 1
        
