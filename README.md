# MASTODON - Borrar los favoritos mas antiguos que la fecha pasada

Borrar los favoritos mas antiguos que la fecha pasada

Sino quieres ejecutarlo en el interprete puedes usar la versión compilada:

[https://gitlab.com/LaCuriosidadDelGato/mastodon-borrar-los-favoritos-mas-antiguos-que-la-fecha-pasada/-/blob/main/pyinstaller.7z](https://gitlab.com/LaCuriosidadDelGato/mastodon-borrar-los-favoritos-mas-antiguos-que-la-fecha-pasada/-/blob/main/pyinstaller.7z)

## Versión compilada en un .exe 

En la carpeta  [pyinstaller](https://gitlab.com/LaCuriosidadDelGato/mastodon-borrar-los-favoritos-mas-antiguos-que-la-fecha-pasada/-/tree/main/pyinstaller) está el código fuente con el que se ha creado el .exe 

Para que funcione hay que modificar los datos de tu cuenta en el archivo datos.txt. Cada dato tiene que estar en una línea independiente y en el mismo orden

- LINEA1: url de la instancia
- LINEA2: correo usado para crear la cuenta
- LINEA3: contraseña de la cuenta
- LINEA4: cantidad de dias de antiguedad para que se elimine
- LINEA5: segundos de espera entre peticiones para no saturar la API
- LINEA6: Cuantos bloques de 40 fav se borran antes de acabar el programa

Para compilar un .exe básico:
```
pyinstaller --onefile main.py
```

## El antivirus lo detecta como virus

Mas información en:

https://github.com/pyinstaller/pyinstaller/issues/5854

## Librerías externas

Se utiliza la librería externa de [Mastodon.py](https://github.com/halcy/Mastodon.py)
```
pip3 install Mastodon.py
```

Si quieres compilarlo en un .exe se usa la librería externa pyinstaller
Para instalar:
```
pip install pyinstaller
```
## Parametros
- API_BASE_URL: url de la instancia
- CUENTA_CORREO: correo usado para crear la cuenta
- CUENTA_PASS: contraseña de la cuenta
- ANTIGUEDAD_DIAS: cantidad de dias de antiguedad para que se elimine
- RETARDO_SEG: segundos de espera entre peticiones para no saturar la API
- SALIR: Cuantos bloques de 40 fav se borran antes de acabar el programa

Ejecutar todas las veces que hagan falta hasta que se vea que ya no borra ningún favorito


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:82741a6f4f9e0641a60bf4d2694cf8e5?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:82741a6f4f9e0641a60bf4d2694cf8e5?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:82741a6f4f9e0641a60bf4d2694cf8e5?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/LaCuriosidadDelGato/mastodon-borrar-los-favoritos-mas-antiguos-que-la-fecha-pasada.git
git branch -M main
git push -uf origin main
```

