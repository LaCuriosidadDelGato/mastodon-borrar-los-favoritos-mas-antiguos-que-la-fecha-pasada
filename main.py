# -*- coding: utf-8 -*-

import datetime
import time
import os

# Descargar libreria Mastodon en: https://github.com/halcy/Mastodon.py
# pip3 install Mastodon.py
from mastodon import Mastodon
 
API_BASE_URL = 'https://mastodon.social'        # url de la instancia
CUENTA_CORREO = 'tucuenta@tuservidor'           # correo usado para crear la cuenta
CUENTA_PASS = 'contraseña'                      # contraseña de la cuenta
ANTIGUEDAD_DIAS = 6 * 30                        # cantidad de dias de antiguedad para que se elimine
RETARDO_SEG = 0.3                               # segundos de espera entre peticiones para no saturar la API
SALIR = 3                                       # Cuantos bloques de 40 fav se borran antes de acabar el programa
 
if not os.path.exists('pytooter_clientcred.secret'):
    Mastodon.create_app(
        'pytooterapp',
        api_base_url = API_BASE_URL,
        to_file = 'pytooter_clientcred.secret'
    )
 
mastodon = Mastodon(
    client_id = 'pytooter_clientcred.secret',
    api_base_url = API_BASE_URL
)
 
mastodon.log_in(
    CUENTA_CORREO,
    CUENTA_PASS,
    to_file = 'pytooter_usercred.secret'
)

fechaRestada = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=int(ANTIGUEDAD_DIAS))
vueltas = 0

while SALIR>vueltas:
    resp = mastodon.favourites(limit=40, min_id=0)
    for fav in resp[::-1]:
        print('')
        print(fav.id)
        print(fav.created_at)
        if fav.created_at < fechaRestada:
            print('El fav es mas antiguo que:')
            print(fechaRestada)
            # a borrar!
            mastodon.status_unfavourite(fav.id)
        else:
            vueltas = SALIR
            break
        time.sleep(RETARDO_SEG)
    vueltas = vueltas + 1
